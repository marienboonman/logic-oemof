# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from oemof.network import *
from oemof.energy_system import *

# create the energy system
es = EnergySystem()

# create bus 1
bus_1 = Bus(label="bus_1")

# create bus 2
bus_2 = Bus(label="bus_2")

# add bus 1 and bus 2 to energy system
es.add(bus_1, bus_2)

# create and add sink 1 to energy system
es.add(Sink(label='sink_1', inputs={bus_1: []}))

# create and add sink 2 to energy system
es.add(Sink(label='sink_2', inputs={bus_2: []}))

# create and add source to energy system
es.add(Source(label='source', outputs={bus_1: []}))

# create and add transformer to energy system
es.add(Transformer(label='transformer', inputs={bus_1: []}, outputs={bus_2: []}))